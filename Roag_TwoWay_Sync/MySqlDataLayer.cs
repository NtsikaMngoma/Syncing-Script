﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Roag_TwoWay_Sync
{
    public class MySqlDataLayer
    {
        private static ConnectionStringSettingsCollection connection = ConfigurationManager.ConnectionStrings;
        public MySqlDataLayer() { }

        public static object GetScalar(string sqlStatement, string sDatabase)
        {
            object result = null;
            using (MySqlConnection con = new MySqlConnection(connection[sDatabase].ConnectionString))
            {
                con.Open();
                using (MySqlCommand com = new MySqlCommand(sqlStatement, con))
                {
                    result = com.ExecuteScalar();
                }
                con.Close();
            }
            return result;
        }

        public static DataTable GetDataSet(string sqlStatement, string sDatabase)
        {
            DataTable result = new DataTable();
            using (MySqlConnection con = new MySqlConnection(connection[sDatabase].ConnectionString))
            {
                con.Open();
                using (MySqlDataAdapter da = new MySqlDataAdapter(sqlStatement, con))
                {
                    da.Fill(result);
                }
                con.Close();
            }
            return result;
        }
    }
}
