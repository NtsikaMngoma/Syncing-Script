﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Roag_TwoWay_Sync
{
    public class DataSync
    {
        Timer _timer = new Timer();
        ConnectionStringSettingsCollection connectionStrings = ConfigurationManager.ConnectionStrings;
        NameValueCollection settings = ConfigurationManager.AppSettings;

        static int _pageSize;

        public DataSync()
        {
            ShowMessage("Initializing.", ConsoleColor.Green);

            _timer.Interval = MakeTimeSpan("SyncIntervalHMS").TotalMilliseconds;
            _timer.Elapsed += _timer_Elapsed;

            _pageSize = Convert.ToInt32(settings["SyncPageSize"]);

            // wait before doing anything, in case we need to quit the program early
            System.Threading.Thread.Sleep(5000);

            ShowMessage($"Starting Data Sync.. polling at {_timer.Interval} milliseconds.", ConsoleColor.Green);
            _timer.Start();
            _timer_Elapsed(null, null);
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            ShowMessage("Starting sync.", ConsoleColor.Green);

            try
            {
                DateTime dsStart = DateTime.Now;


                DataTable dtLocalUsers = DataLayer.GetDataSetMssql("select * from users", "LocalDatabase");
                //DataTable dtLocalRemoteEntries = DataLayer.GetDataSetMssql("select * from remote_entries", "LocalDatabase");
                DataTable dtLocalRaceEntries = DataLayer.GetDataSetMssql("select * from race_entries", "LocalDatabase");
                DataTable dtLocalRaceEntryAnswer = DataLayer.GetDataSetMssql("select * from race_entry_answer", "LocalDatabase");
                DataTable dtLocalRaceResult = DataLayer.GetDataSetMssql("select * from race_result", "LocalDatabase");
                DataTable dtLocalRiderDetails =
                    DataLayer.GetDataSetMssql("select * from rider_details", "LocalDatabase");
                DataTable dtLocalEventQuestions =
                    DataLayer.GetDataSetMssql("select * from event_questions", "LocalDatabase");
                DataTable dtLocalEventRaceBatch =
                    DataLayer.GetDataSetMssql("select * from event_race_batch", "LocalDatabase");
                DataTable dtLocalEventRaceDiscount =
                    DataLayer.GetDataSetMssql("select * from event_race_discount", "LocalDatabase");
                DataTable dtLocalEventRace =
                    DataLayer.GetDataSetMssql("select * from event_race", "LocalDatabase");
                DataTable dtLocalEventRacePrice =
                    DataLayer.GetDataSetMssql("select * from event_race_price", "LocalDatabase");
                DataTable dtLocalEventRaceQuestion =
                    DataLayer.GetDataSetMssql("select * from event_race_question", "LocalDatabase");
                DataTable dtLocalEventRaceRating =
                    DataLayer.GetDataSetMssql("select * from event_race_rating", "LocalDatabase");
                DataTable dtLocalMerchandise =
                    DataLayer.GetDataSetMssql("select * from merchandise", "LocalDatabase");
                DataTable dtLocalPayment =
                    DataLayer.GetDataSetMssql("select * from payment", "LocalDatabase");
                DataTable dtLocalPurchase =
                    DataLayer.GetDataSetMssql("select * from purchase", "LocalDatabase");
                
                //Mappings of all the classes
                Mappings mappings = new Mappings();

                DataTable dtRemoteUsers = mappings.MapUsers(dtLocalUsers);
                DataTable dtRemoteRaceEntries = Mappings.RaceEntries(dtLocalRaceEntries);
                DataTable dtRemoteRaceEntryAnswer = Mappings.RaceEntryAnswer(dtLocalRaceEntryAnswer);
                DataTable dtRemoteRaceResult = Mappings.RaceResult(dtLocalRaceResult);
                DataTable dtRemoteRiderDetails = Mappings.RiderDetails(dtLocalRiderDetails);
                DataTable dtRemoteEventQuestions = Mappings.EventQuestions(dtLocalEventQuestions);
                DataTable dtRemoteEventRaceBatch = Mappings.EventRaceBatch(dtLocalEventRaceBatch);
                DataTable dtRemoteEventRaceDiscount = Mappings.EventRaceDiscount(dtLocalEventRaceDiscount);
                DataTable dtRemoteEventRace = Mappings.EventRace(dtLocalEventRace);
                DataTable dtRemoteEventRacePrice = Mappings.EventRacePrice(dtLocalEventRacePrice);
                DataTable dtRemoteEventRaceQuestion = Mappings.EventRaceQuestion(dtLocalEventRaceQuestion);
                DataTable dtRemoteEventRaceRating = Mappings.EventRaceRating(dtLocalEventRaceRating);
                DataTable dtRemoteMerchandise = Mappings.Merchandise(dtLocalMerchandise);
                DataTable dtRemotePayment = Mappings.Payment(dtLocalPayment);
                DataTable dtRemotePurchase = Mappings.Purchase(dtLocalPurchase);
                
                //Inserting data into the data tables
                DataLayer.InsertMySqlData(dtRemoteUsers, "users", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteRaceEntries, "race_entries", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteRaceEntryAnswer, "race_entry_answer", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteRaceResult, "race_result", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteRiderDetails, "rider_details", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteEventQuestions, "event_questions", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteEventRaceBatch, "event_race_batch", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteEventRaceDiscount, "event_race_discount", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteEventRace, "event_race", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteEventRacePrice, "event_race_price", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteEventRaceQuestion, "event_race_question", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteEventRaceRating, "event_race_rating", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemoteMerchandise, "merchandise", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemotePayment, "payment", "RemoteDatabase");
                DataLayer.InsertMySqlData(dtRemotePurchase, "purchase", "RemotePurchase");
                
                // sample sql code with skip/take
                /*string sql = $@"
                    SELECT *
                    FROM Sales.SalesOrderHeader 
                    ORDER BY OrderDate
                    OFFSET ({transactionPageIndex * _pageSize}) ROWS FETCH NEXT ({_pageSize}) ROWS ONLY
                 ";*/




                #region transaction data

                ShowMessage("Syncing Transaction data.", ConsoleColor.Green);

                // get the latest LAST_MOD date from our side
                string strLocalTimestamp = MsSqlDataLayer.GetScalar("SELECT Max(TIMESTAMP) as TIMESTAMP FROM TRANSACTIONS", "LocalDatabase").ToString();
                DateTime dtLocalTimestamp;
                DateTime.TryParse(strLocalTimestamp, out dtLocalTimestamp);
                ShowMessage($"Latest transaction timestamp is {dtLocalTimestamp.ToString("yyyy-MM-dd HH:mm:ss")}.", ConsoleColor.Green);

                // check how many new records we have to deal with
                string sqlHowManyTrans = $@"
                    SELECT Count(*) as TOTAL
                    FROM TRANSACTIONS 
                    WHERE TIMESTAMP > TO_DATE('{dtLocalTimestamp.ToString("yyyy-MM-dd HH:mm:ss")}', 'yyyy-MM-dd HH24:MI:SS')
                ";
                int newTransactionRecords = Convert.ToInt32(MySqlDataLayer.GetScalar(sqlHowManyTrans, "RemoteDatabase").ToString());
                int transactionPageIndex = 0;
                int transactionRowsCopied = 0;
                ShowMessage($"There are {newTransactionRecords} new rows..", ConsoleColor.Green);

                if (newTransactionRecords > 0)
                {
                    while (transactionRowsCopied < newTransactionRecords)
                    {
                        // fetch all the remote data that is newer than the local data
                        string sqlGetNewTransactions = $@"
                            SELECT *
                            FROM   (SELECT *
                                    FROM   (SELECT tr.*, rownum AS rnum
                                            FROM   TRANSACTIONS tr
                                            WHERE TIMESTAMP > TO_DATE('{dtLocalTimestamp.ToString("yyyy-MM-dd HH:mm:ss")}', 'yyyy-MM-dd HH24:MI:SS')
                                            ORDER BY TIMESTAMP)
                                    WHERE rownum <= {(transactionPageIndex * _pageSize) + _pageSize})
                            WHERE  rnum > {transactionPageIndex * _pageSize}
                        ";
                        //ShowMessage(sqlGetNewTransactions, ConsoleColor.Yellow);
                        DataTable dtNewTransactionData = MySqlDataLayer.GetDataSet(sqlGetNewTransactions, "RemoteDatabase");

                        // we have the extra rnum column from above, which we need to remove since we don't want it in the local database
                        dtNewTransactionData.Columns.RemoveAt(dtNewTransactionData.Columns.Count - 1);

                        // copy any new data to the local db
                        CopyToLocal(dtNewTransactionData, "TRANSACTIONS");
                        transactionRowsCopied += dtNewTransactionData.Rows.Count;
                        transactionPageIndex++;

                        ShowMessage($"Rows copied: {transactionRowsCopied}.", ConsoleColor.Green);
                    }
                }
                #endregion


                #region customer data
                ShowMessage("Syncing Customer data.", ConsoleColor.Green);
                int customerRowsAdded = 0, customerRowsUpdated = 0, customerRowsProcessed = 0;

                // get a list of customers on their side
                DataTable dtRemoteCustomers = MySqlDataLayer.GetDataSet("SELECT ACCOUNTNUMBER FROM CUSTOMER_DATA", "RemoteDatabase");
                foreach (DataRow drCust in dtRemoteCustomers.Rows)
                {
                    string accountNumber = drCust["ACCOUNTNUMBER"].ToString();

                    // get the latest LAST_MOD date from our side for this account (we will store multiple records per account, to keep track of the changes)
                    string strLocalLastMod = MsSqlDataLayer.GetScalar($"SELECT Max(LAST_MOD) as LAST_MOD FROM CUSTOMER_DATA WHERE ACCOUNTNUMBER = '{accountNumber}'", "LocalDatabase").ToString();
                    DateTime dtLocalLastMod;
                    DateTime.TryParse(strLocalLastMod, out dtLocalLastMod);

                    // get the LAST_MOD date on their side for this account
                    string strRemoteLastMod = MySqlDataLayer.GetScalar($"SELECT LAST_MOD FROM CUSTOMER_DATA WHERE ACCOUNTNUMBER = '{accountNumber}'", "RemoteDatabase").ToString();
                    DateTime dtRemoteLastMod;
                    DateTime.TryParse(strRemoteLastMod, out dtRemoteLastMod);

                    // if the account exists on our side, and, our account has the same or a newer date, ignore it
                    if (strLocalLastMod != "" && dtLocalLastMod >= dtRemoteLastMod)
                    {
                        ShowMessage($"[{customerRowsProcessed}] Account {accountNumber} is up-to-date.", ConsoleColor.Green);
                    }
                    // if we don't have the account, or if our account is older, pull new data
                    else if (strLocalLastMod == "" || (strLocalLastMod != "" && dtLocalLastMod < dtRemoteLastMod))
                    {
                        // fetch the LEC data for this account
                        DataTable dtNewCustomerData = MySqlDataLayer.GetDataSet($"SELECT * FROM CUSTOMER_DATA WHERE ACCOUNTNUMBER = '{accountNumber}'", "RemoteDatabase");

                        // copy the new data to the local db
                        CopyToLocal(dtNewCustomerData, "CUSTOMER_DATA");

                        if (strLocalLastMod == "")
                        {
                            customerRowsAdded++;
                            ShowMessage($"[{customerRowsProcessed}] Account {accountNumber} added.", ConsoleColor.Green);
                        }
                        else
                        {
                            customerRowsUpdated++;
                            ShowMessage($"[{customerRowsProcessed}] Account {accountNumber} updated.", ConsoleColor.Green);
                        }
                    }

                    customerRowsProcessed++;
                }
                ShowMessage($"Customers Added: {customerRowsAdded}, Updated: {customerRowsUpdated}, Total Processed: {customerRowsProcessed}", ConsoleColor.Green);
                #endregion


                ShowMessage($"Sync finished - took {(DateTime.Now - dsStart).TotalSeconds} seconds", ConsoleColor.Green);

                _timer.Interval = MakeTimeSpan("SyncIntervalHMS").TotalMilliseconds;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.ToString(), ConsoleColor.Red);
                _timer.Interval = MakeTimeSpan("RetryIntervalHMS").TotalMilliseconds;
                ShowMessage($"Retrying in {_timer.Interval / 1000} seconds", ConsoleColor.Yellow);
            }

            _timer.Start();
        }

        private void CopyToLocal(DataTable dtNewData, string destinationTableName)
        {
            using (SqlConnection con = new SqlConnection(connectionStrings["LocalDatabase"].ConnectionString))
            {
                con.Open();
                using (SqlBulkCopy copy = new SqlBulkCopy(con))
                {
                    copy.DestinationTableName = destinationTableName;

                    // use one of the column mappings overloads here if you need
                    //copy.ColumnMappings.Add();

                    copy.WriteToServer(dtNewData);
                }
                con.Close();
            }
        }

        public static void ShowMessage(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + ": " + message);
            Console.ForegroundColor = ConsoleColor.White;
        }

        TimeSpan MakeTimeSpan(string setting)
        {
            string settinValue = settings[setting];
            int[] intSetting = GetHms(settinValue);
            return new TimeSpan(intSetting[0], intSetting[1], intSetting[2]);
        }

        static int[] GetHms(string setting)
        {
            string[] temp = setting.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            int[] result = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
                result[i] = Convert.ToInt32(temp[i]);
            return result;
        }
    }
}
