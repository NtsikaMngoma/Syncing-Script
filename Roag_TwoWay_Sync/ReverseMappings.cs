using System;
using System.Data;

namespace Roag_TwoWay_Sync
{
    public class ReverseMappings
    {
        public DataTable MapUsers(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            // set up the db structure of the mysql db
            /*dtNewData.Columns.Add("ID", typeof(string));
            dtNewData.Columns.Add("FirstName", typeof(string));
            dtNewData.Columns.Add("LastName", typeof(string));
            dtNewData.Columns.Add("IdentityNo", typeof(string));*/

            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var store = Utils.Encrypt(null, true);
                var decrypt = Utils.Decrypt(null, false);
                
                DataRow newDr = dtNewData.NewRow();
                // the mappings
                // remote column = local column
                newDr["id"] = 1 * (int) drOldWebsite["RoagID"];
                newDr["firstname"] = drOldWebsite["FirstName"];
                newDr["lastname"] = drOldWebsite["LastName"];
                newDr["id_number"] = drOldWebsite["IDNumber"];
                newDr["email"] = drOldWebsite["Email"];
                newDr["password"] = drOldWebsite["Password"];
                newDr["confimation_code"] = GetUniqID();
                newDr["confirmed"] = true;
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = drOldWebsite["LastUpdated"];
                newDr["gender_id"] = drOldWebsite["GenderID"];
                newDr["dob"] = drOldWebsite["DOB"];
                newDr["day_contact"] = (drOldWebsite["DayContact"]).ToString().Trim();
                newDr["day_contact_tel"] = drOldWebsite["DayContactTel"];
                newDr["day_building_complex"] = (drOldWebsite["DayBuildingComplex"]).ToString().Trim();
                newDr["day_street"] = (drOldWebsite["DayStreet"]);
                newDr["day_suburb_id"] = drOldWebsite["DaySuburbID"];
                newDr["day_town_id"] = drOldWebsite["DaytownID"];
                newDr["day_province_id"] = (int) drOldWebsite["DayProvinceID"] > 0  ? drOldWebsite["DayProvinceID"] : 10;
                newDr["day_country_id"] = drOldWebsite["DayCountryID"];
                newDr["day_postcode"] = drOldWebsite["DayPostCode"];
                newDr["postal_1"] = (drOldWebsite["Postal1"]).ToString().Trim();
                newDr["postal_2"] = (drOldWebsite["Postal2"]).ToString().Trim();
                newDr["postcode"] = drOldWebsite["Postcode"];
                newDr["suburb_id"] = drOldWebsite["SuburbID"];
                newDr["town_id"] = drOldWebsite["TownID"];
                newDr["province_id"] = (int) drOldWebsite["ProvinceID"] > 0 ? drOldWebsite["ProvinceID"] : 10;
                newDr["country_id"] = drOldWebsite["CountryID"];
                newDr["tel_cell"] = drOldWebsite["TelCell"];
                newDr["tel_work"] = drOldWebsite["TelWork"];
                newDr["tel_home"] = drOldWebsite["TelHome"];
                newDr["fax"] = drOldWebsite["Fax"];
                newDr["med_aid_name"] = (drOldWebsite["MedAidName"]).ToString().Trim();
                newDr["med_aid_no"] = drOldWebsite["MedAidNo"];
                newDr["med_aid_id"] = drOldWebsite["MedAidID"];
                newDr["blood_type_id"] = drOldWebsite["BloodTypeID"];
                newDr["em_contact"] = (drOldWebsite["EmContact"]).ToString().Trim();
                newDr["em_contact_no"] = drOldWebsite["EmContactNo"];
                newDr["em_contact_2"] = drOldWebsite["EmContact2"];
                newDr["em_contact_2_no"] = drOldWebsite["EmContactNo2"];
                newDr["doctor"] = (drOldWebsite["Doctor"]).ToString().Trim();
                newDr["doctor_tel"] = drOldWebsite["DoctorTel"];
                newDr["allergies"] = drOldWebsite["Allergies"];
                newDr["medical_conditions"] = drOldWebsite["MedicalCondtions"];
                newDr["club_roag"] = drOldWebsite["ClubRoag"];
                newDr["cycling_club_id"] = drOldWebsite["CyclingClubID"];
                newDr["csa_no"] = drOldWebsite["CSA"];
                newDr["canoeing_club_id"] = drOldWebsite["CanoeingClubID"];
                newDr["canoeing_sa_no"] = drOldWebsite["CaSaNo"];
                newDr["running_club_id"] = drOldWebsite["RunningClubID"];
                newDr["asa_no"] = drOldWebsite["ASANo"];
                newDr["swimming_club_id"] = drOldWebsite["SwimmingClubID"];
                newDr["swim_sa_no"] = drOldWebsite["SwimSANo"];
                newDr["trail_run_club_id"] = drOldWebsite["TrailRunClubID"];
                newDr["trail_sa_no"] = drOldWebsite["TRASANo"];
                newDr["school_id"] = drOldWebsite["SchoolID"];
                newDr["school_name"] = (drOldWebsite["SchoolName"]).ToString().Trim();
                newDr["champion_chip"] = drOldWebsite["ChampionChip"];
                newDr["winning_time"] = drOldWebsite["WinningTime"];
                newDr["elite"] = drOldWebsite["Elite"];
                newDr["spectrum"] = drOldWebsite["Spectrum"];
                newDr["ppa"] = drOldWebsite["PPA"];
                newDr["avendurance"] = drOldWebsite["Avendurance"];
                newDr["sa_seeding"] = drOldWebsite["SASeeding"];
                newDr["shirt_size_id"] = drOldWebsite["ShirtSizeID"];
                newDr["guardian_type_id"] = drOldWebsite["gaurdianTypeID"];
                newDr["guardian_name"] = (drOldWebsite["GaurdianName"]).ToString().Trim();
                newDr["guardian_surname"] = (drOldWebsite["GaurdianSurname"]).ToString().Trim();
                newDr["guardian_id"] = drOldWebsite["GaurdianID"];
                newDr["guardian_val"] = drOldWebsite["GaurdianVal"];
                newDr["auto_generate"] = drOldWebsite["Autogenerate"];
                newDr["allow_email"] = drOldWebsite["AllowEmail"];
                newDr["number_board"] = drOldWebsite["NumberBoard"];
                newDr["roag_chip"] = drOldWebsite["RoagChip"];
                newDr["roag_chip_no"] = drOldWebsite["RoagchipNo"];
                newDr["register_date"] = drOldWebsite["RegisterDate"];
                newDr["avatar"] = drOldWebsite["Avatar"];
                newDr["allow_em_contact_info"] = drOldWebsite["AllowwEmContactInfo"];
                newDr["rec_newsletter"] = drOldWebsite["RecNewsletter"];
                newDr["team_vitality_member"] = drOldWebsite["TeamVitalityMember"];
                newDr["send_data_to_vitality"] = drOldWebsite["SendDataToVitality"];



                

                //string oldPwdHashed = drOldWebsite["IdentityNo"].ToString();
                //public static string plainPassword = 
                //string newPwdHash = 
                
                //newDr["Password"] = newPwdHash;
            }

            return dtOldData;
        }

        public DataTable RaceEntries(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();

            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                DataRow newDr = dtNewData.NewRow();

                newDr["id"] = drOldWebsite["RaceEntryID"];
                newDr["race_id"] = drOldWebsite["RaceID"];
                newDr["user_id"] = drOldWebsite["RoagID"];
                newDr["payment_id"] = drOldWebsite["PaymentID"];
                newDr["price"] = drOldWebsite["Price"];
                newDr["currency_id"] = drOldWebsite["CurrencyID"];
                newDr["conversion_rate"] = drOldWebsite["ConversionRate"];
                newDr["team_id"] = drOldWebsite["TeamID"];
                newDr["registered"] = drOldWebsite["Registered"];
                newDr["batch_name"] = drOldWebsite["Batch"];
                newDr["batch_id"] = drOldWebsite["BatchID"];
                newDr["start_time"] = drOldWebsite["StartTime"];
                newDr["discount_voucher_code"] = drOldWebsite["DiscountVoucher"];
                newDr["discount_percent"] = drOldWebsite["DiscountPercent"];
                newDr["nett_amount"] = drOldWebsite["Amount"];
                newDr["team_name"] = drOldWebsite["TeamName"];
                newDr["age_group_id"] = drOldWebsite["AgeGroupID"];
                newDr["is_team_entrant_amount"] = drOldWebsite["TeamEntrantAmount"];
                newDr["is_paid"] = drOldWebsite["Paid"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }
            return dtOldData;
        }

        public DataTable RaceEntryAnswer(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                DataRow newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["AnswerID"];
                newDr["race_entry_id"] = drOldWebsite["RaceEntryID"];
                newDr["event_question_id"] = drOldWebsite["QuestionID"];
                newDr["answer"] = drOldWebsite["Answer"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }
            return dtOldData;
        }

        public DataTable RaceResult(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["ResultID"];
                newDr["race_id"] = drOldWebsite["RaceID"];
                newDr["rider_id"] = drOldWebsite["RoagID"];
                newDr["race_time"] = drOldWebsite["RaceTime"];
                newDr["race_position"] = drOldWebsite["RacePosition"];
                newDr["user_input"] = drOldWebsite["UserInput"];
                newDr["ranking"] = drOldWebsite["Ranking"];
                newDr["adjusted_index"] = drOldWebsite["AdjustedIndex"];
            }
            return dtOldData;
        }

        public DataTable RiderDetails(DataTable dtOldData)
        {
            return dtOldData;
        }

        public DataTable EventQuestions(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["QuestionID"];
                newDr["event_id"] = drOldWebsite["EventID"];
                newDr["question"] = drOldWebsite["Question"];
                newDr["mandatory"] = drOldWebsite["Mandatory"];
                newDr["enabled"] = drOldWebsite["Enabled"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }

            return dtOldData;
        }

        public DataTable EventRaceBatch(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["BatchID"];
                newDr["race_id"] = drOldWebsite["RaceID"];
                newDr["batch"] = drOldWebsite["Batch"];
                newDr["description"] = drOldWebsite["Description"];
                newDr["start_time"] = drOldWebsite["StartTime"];
                newDr["max_entries"] = drOldWebsite["MaxEntries"];
                newDr["late_batch"] = drOldWebsite["LateBatch"];
                newDr["enabled"] = drOldWebsite["Enabled"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }

            return dtOldData;
        }

        public DataTable EventRaceDiscount(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["VoucherID"];
                newDr["event_id"] = drOldWebsite["EventID"];
                newDr["race_id"] = drOldWebsite["RaceID"];
                newDr["discount_percent"] = drOldWebsite["DiscountPercent"];
                newDr["discount_voucher"] = drOldWebsite["DiscountVoucher"];
                newDr["issued_to"] = drOldWebsite["IssuedTo"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }

            return dtOldData;
        }
        
        public DataTable EventRace(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["RaceID"];
                newDr["event_id"] = drOldWebsite["EventID"];
                newDr["race"] = drOldWebsite["Race"];
                newDr["distance"] = drOldWebsite["Distance"];
                newDr["start_date_time"] = drOldWebsite["StartDateTime"];
                newDr["team_event"] = drOldWebsite["TeamEvent"];
                newDr["min_team"] = drOldWebsite["MinTeam"];
                newDr["max_team"] = drOldWebsite["MaxTeam"];
                newDr["price"] = drOldWebsite["Price"];
                newDr["eft_price"] = drOldWebsite["EFTPrice"];
                newDr["eft_only"] = drOldWebsite["EFTOnly"];
                newDr["late_entry"] = drOldWebsite["LateEntry"];
                newDr["late_entry_fee"] = drOldWebsite["LateEntryFee"];
                newDr["race_detail"] = drOldWebsite["RaceDetail"];
                newDr["min_age"] = drOldWebsite["MinAge"];
                newDr["max_age"] = drOldWebsite["MaxAge"];
                newDr["license_check"] = drOldWebsite["LicenseCheck"];
                newDr["license_type_id"] = drOldWebsite["LicenseTypeID"];
                newDr["stage_race"] = drOldWebsite["StageRace"];
                newDr["duration"] = drOldWebsite["Duration"];
                newDr["enabled"] = drOldWebsite["Enabled"];
                newDr["CSA_id"] = drOldWebsite["CSA_ID"];
                newDr["price_amended"] = drOldWebsite["PriceAmended"];
                newDr["date_price_amended"] = drOldWebsite["DatePriceAmended"];
                newDr["price_amended2"] = drOldWebsite["PriceAmended2"];
                newDr["date_price_amended2"] = drOldWebsite["DatePriceAmended2"];
                newDr["price_amended3"] = drOldWebsite["PriceAmended3"];
                newDr["date_price_amended3"] = drOldWebsite["DatePriceAmended3"];
                newDr["promo_code"] = drOldWebsite["PromoCode"];
                newDr["max_entries"] = drOldWebsite["MaxEntries"];
                newDr["show_entries_count"] = drOldWebsite["ShowEntriesCount"];
                newDr["age_group_set_id"] = drOldWebsite["AgeGroupSetID"];
                newDr["age_type_id"] = drOldWebsite["AgeTypeTypeID"];
                newDr["participants"] = drOldWebsite["Participants"];
                newDr["competitiors"] = drOldWebsite["Competitors"];
                newDr["correlation"] = drOldWebsite["Correlation"];
                newDr["race_difficulty"] = drOldWebsite["RaceDifficulty"];
                newDr["field_quality"] = drOldWebsite["FieldQuality"];
                newDr["race_factor"] = drOldWebsite["RaceFactor"];
                newDr["winning_time"] = drOldWebsite["WinnningTime"];
                newDr["category"] = drOldWebsite["Category"];
                newDr["email_attachment_filename"] = drOldWebsite["EmailAttchmentFilename"];
                newDr["email_attachment"] = drOldWebsite["EmailAttahcment"];
                newDr["rider_select_batch"] = drOldWebsite["RiderSelectBatch"];
                newDr["online_entry"] = drOldWebsite["OnlineEntry"];
                newDr["team_price"] = drOldWebsite["TeamPrice"];
                newDr["overall_prizes"] = drOldWebsite["OverallPrizes"];
                newDr["overall_positions"] = drOldWebsite["OverallPositions"];
                newDr["overall_prizes_min_age"] = drOldWebsite["OverallPrizesMinAge"];
                newDr["overall_prizes_max_age"] = drOldWebsite["OverallPriceMaxRange"];
                newDr["required_proficiency"] = drOldWebsite["RequiredProficiency"];
                newDr["team_name_auto_generate"] = drOldWebsite["TeamNameAutoGenerate"];
                newDr["document_required"] = drOldWebsite["DocumentRequired"];
                newDr["document_description"] = drOldWebsite["DocumentDescription"];
                newDr["allow_discount"] = drOldWebsite["AllowDiscount"];
                newDr["race_color"] = drOldWebsite["RaceColor"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }

            return dtOldData;
        }
        
        public DataTable EventRacePrice(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["BatchID"];
                newDr["race_id"] = drOldWebsite["RaceID"];
                newDr["batch"] = drOldWebsite["Batch"];
                newDr["description"] = drOldWebsite["Description"];
                newDr["start_time"] = drOldWebsite["StartTime"];
                newDr["max_entries"] = drOldWebsite["MaxEntries"];
                newDr["late_batch"] = drOldWebsite["LateBatch"];
                newDr["enabled"] = drOldWebsite["Enabled"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }

            return dtOldData;
        }
        
        public DataTable EventRaceQuestion(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["BatchID"];
                newDr["race_id"] = drOldWebsite["RaceID"];
                newDr["batch"] = drOldWebsite["Batch"];
                newDr["description"] = drOldWebsite["Description"];
                newDr["start_time"] = drOldWebsite["StartTime"];
                newDr["max_entries"] = drOldWebsite["MaxEntries"];
                newDr["late_batch"] = drOldWebsite["LateBatch"];
                newDr["enabled"] = drOldWebsite["Enabled"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }

            return dtOldData;
        }
        
        public DataTable EventRaceRating(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["BatchID"];
                newDr["race_id"] = drOldWebsite["RaceID"];
                newDr["batch"] = drOldWebsite["Batch"];
                newDr["description"] = drOldWebsite["Description"];
                newDr["start_time"] = drOldWebsite["StartTime"];
                newDr["max_entries"] = drOldWebsite["MaxEntries"];
                newDr["late_batch"] = drOldWebsite["LateBatch"];
                newDr["enabled"] = drOldWebsite["Enabled"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }

            return dtOldData;
        }
        
        public DataTable Merchandise(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["BatchID"];
                newDr["race_id"] = drOldWebsite["RaceID"];
                newDr["batch"] = drOldWebsite["Batch"];
                newDr["description"] = drOldWebsite["Description"];
                newDr["start_time"] = drOldWebsite["StartTime"];
                newDr["max_entries"] = drOldWebsite["MaxEntries"];
                newDr["late_batch"] = drOldWebsite["LateBatch"];
                newDr["enabled"] = drOldWebsite["Enabled"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }

            return dtOldData;
        }
        
        public DataTable Payment(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["BatchID"];
                newDr["race_id"] = drOldWebsite["RaceID"];
                newDr["batch"] = drOldWebsite["Batch"];
                newDr["description"] = drOldWebsite["Description"];
                newDr["start_time"] = drOldWebsite["StartTime"];
                newDr["max_entries"] = drOldWebsite["MaxEntries"];
                newDr["late_batch"] = drOldWebsite["LateBatch"];
                newDr["enabled"] = drOldWebsite["Enabled"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }

            return dtOldData;
        }
        
        
        public DataTable Purchase(DataTable dtOldData)
        {
            DataTable dtNewData = new DataTable();
            foreach (DataRow drOldWebsite in dtOldData.Rows)
            {
                var newDr = dtNewData.NewRow();
                newDr["id"] = drOldWebsite["BatchID"];
                newDr["race_id"] = drOldWebsite["RaceID"];
                newDr["batch"] = drOldWebsite["Batch"];
                newDr["description"] = drOldWebsite["Description"];
                newDr["start_time"] = drOldWebsite["StartTime"];
                newDr["max_entries"] = drOldWebsite["MaxEntries"];
                newDr["late_batch"] = drOldWebsite["LateBatch"];
                newDr["enabled"] = drOldWebsite["Enabled"];
                newDr["created_at"] = DateTime.Now;
                newDr["updated_at"] = DateTime.Now;
            }

            return dtOldData;
        }
        
        private string GetUniqID()
        {
            var ts = (DateTime.UtcNow - new DateTime());
            double t = ts.TotalMilliseconds / 1000;
        
            int a = (int)Math.Floor(t);
            int b = (int)((t - Math.Floor(t)) * 1000000);
        
            return a.ToString("x8") + b.ToString("x5");
        }
    }
}