﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roag_TwoWay_Sync
{
    public class MsSqlDataLayer
    {
        private static readonly ConnectionStringSettingsCollection Connection = ConfigurationManager.ConnectionStrings;
        public MsSqlDataLayer() { }

        public static object GetScalar(string sqlStatement, string sDatabase)
        {
            object result = null;
            using (SqlConnection con = new SqlConnection(Connection[sDatabase].ConnectionString))
            {
                con.Open();
                using (SqlCommand com = new SqlCommand(sqlStatement, con))
                {
                    result = com.ExecuteScalar();
                }
                con.Close();
            }
            return result;
        }

        public static DataTable GetDataSet(string sqlStatement, string sDatabase)
        {
            DataTable result = new DataTable();
            using (SqlConnection con = new SqlConnection(Connection[sDatabase].ConnectionString))
            {
                con.Open();
                using (SqlDataAdapter da = new SqlDataAdapter(sqlStatement, con))
                {
                    da.Fill(result);
                }
                con.Close();
            }
            return result;
        }
    }
}
