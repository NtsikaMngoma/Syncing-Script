﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roag_TwoWay_Sync
{
    public class DataLayer
    {
        private static ConnectionStringSettingsCollection ConnectionStrings = ConfigurationManager.ConnectionStrings;


        public static DataTable GetDataSetMssql(string sqlStatement, string sDatabase)
        {
            DataTable result = new DataTable();
            using (SqlConnection con = new SqlConnection(ConnectionStrings[sDatabase].ConnectionString))
            {
                con.Open();
                using (SqlDataAdapter da = new SqlDataAdapter(sqlStatement, con))
                {
                    da.Fill(result);
                }
                con.Close();
            }
            return result;
        }

        public static void InsertMySqlData(DataTable dtRemoteData, string tableName, string sDatabase)
        {
            using (MySqlConnection con = new MySqlConnection(ConnectionStrings[sDatabase].ConnectionString))
            {
                con.Open();
                using (MySqlDataAdapter da = new MySqlDataAdapter($"select top 1 * from {tableName}", con))
                {
                    MySqlCommandBuilder builder = new MySqlCommandBuilder(da);
                    da.InsertCommand = builder.GetInsertCommand();
                    da.Update(dtRemoteData);
                }
                con.Close();
            }
        }
    }
}
